﻿using System.Security.Cryptography;
using System.Text.Json;
using Microsoft.IdentityModel.Tokens;

namespace Signicat.Playground.Jwe.ConsoleApp
{
    public class RSAParametersString
    {
        public string D { get; set; }
        public string DP { get; set; }
        public string DQ { get; set; }
        public string Exponent { get; set; }
        public string InverseQ { get; set; }
        public string Modulus { get; set; }
        public string P { get; set; }
        public string Q { get; set; }

        public static RSAParametersString ToBase64(RSAParameters rsaParameters)
        {
            var rsaParametersBase64 = new RSAParametersString
            {
                D = Base64UrlEncoder.Encode(rsaParameters.D),
                DP = Base64UrlEncoder.Encode(rsaParameters.DP),
                DQ = Base64UrlEncoder.Encode(rsaParameters.DQ),
                Exponent = Base64UrlEncoder.Encode(rsaParameters.Exponent),
                InverseQ = Base64UrlEncoder.Encode(rsaParameters.InverseQ),
                Modulus = Base64UrlEncoder.Encode(rsaParameters.Modulus),
                P = Base64UrlEncoder.Encode(rsaParameters.P),
                Q = Base64UrlEncoder.Encode(rsaParameters.Q),
            };

            return rsaParametersBase64;
        }

        public static RSAParameters FromBase64Json(string json)
        {
            RSAParametersString rsaParametersBase64 = JsonSerializer.Deserialize<RSAParametersString>(json);

            return FromBase64(rsaParametersBase64);
        }

        public static RSAParameters FromBase64(RSAParametersString rsaParametersBase64)
        {
            var rsaParameters = new RSAParameters
            {
                D = Base64UrlEncoder.DecodeBytes(rsaParametersBase64.D),
                DP = Base64UrlEncoder.DecodeBytes(rsaParametersBase64.DP),
                DQ = Base64UrlEncoder.DecodeBytes(rsaParametersBase64.DQ),
                Exponent = Base64UrlEncoder.DecodeBytes(rsaParametersBase64.Exponent),
                InverseQ = Base64UrlEncoder.DecodeBytes(rsaParametersBase64.InverseQ),
                Modulus = Base64UrlEncoder.DecodeBytes(rsaParametersBase64.Modulus),
                P = Base64UrlEncoder.DecodeBytes(rsaParametersBase64.P),
                Q = Base64UrlEncoder.DecodeBytes(rsaParametersBase64.Q),
            };

            return rsaParameters;
        }
    }
}
